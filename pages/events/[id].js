import { React, useState, useEffect } from "react";
import { useRouter } from "next/router";
import styles from "./id.module.css";

const Events = () => {
  const router = useRouter();
  const { id } = router.query;
  const [e, setEvent] = useState([]);

  useEffect(() => {
    async function getEvents() {
      if (id != undefined) {
        const res = await fetch(`/eventById/${id.toString()}`);
        const data = await res.json();
        setEvent([...data]);
      }
    }
    getEvents();
  }, [id]);

  const checkForEvent = () => {
    const listOfEvents = [];
    for (let ev of e) {
      listOfEvents.push(ev);
    }
    return listOfEvents;
  };

  const deleteEvent = async () => {
    const res = await fetch("/event", {
      body: JSON.stringify({ id }),
      headers: {
        "Content-Type": "application/json",
      },
      method: "DELETE",
    });

    const result = await res.json();

    router.back();
  };

  return (
    <div>
      <h1 className={styles.t}> Detalji sastanka </h1>
      <div className={styles.details}>
        {checkForEvent().map((e) => {
          return (
            <div key={e.title + e.day}>
              <p className={styles.p}> Naziv: {e.title}</p>
              <p className={styles.p}> Datum: {e.day}.8.2022.</p>
              <p className={styles.p}> Vreme: {e.time} </p>
              <p className={styles.p}> Opis: {e.description}</p>
              <div className={styles.p}>
                Ucesnici:
                {e.attendees.join(", ")}
              </div>

              <button className={styles.button} onClick={() => deleteEvent()}>
                Obrisi sastanak
              </button>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Events;
