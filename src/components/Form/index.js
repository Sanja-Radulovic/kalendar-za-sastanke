import styles from "./modal.module.css";
import { MultiSelect } from "react-multi-select-component";
import { useEffect, useState } from "react";
import TimeKeeper from "react-timekeeper";

const Form = (props) => {
  if (!props.show) {
    return null;
  }

  const [title, setTitle] = useState("");
  const [description, setDesc] = useState("");

  const [time, setTime] = useState("12:00pm");

  const [attendees, setAttendees] = useState([]);
  const [selected, setSelected] = useState([]);

  const validateTitle = (title) => !!title;
  const validateDesc = (description) => !!description;

  useEffect(() => {
    async function getAttendees() {
      const res = await fetch("/attendees");
      const data = await res.json();
      setAttendees(data.map((a) => ({ label: a.name, value: a.name })));
    }
    getAttendees();
  }, []);

  const addEvent = async (event) => {
    event.preventDefault();

    const id = props.id;
    const attendees = selected.map((a) => a.value);
    const day = props.day;
    const res = await fetch("/event", {
      body: JSON.stringify({
        id,
        title,
        description,
        day,
        time,
        attendees,
      }),
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
    });
    const result = await res.json();
  };

  return (
    <>
      <div onClick={props.onClose}>
        <div
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <div>
            <form onSubmitCapture={addEvent} onSubmit={props.onClose}>
              <div>
                <div className={styles.overlay}>
                  <div className={styles.modal}>
                    <div>
                      <button onClick={props.onClose} className={styles.btn1}>
                        X
                      </button>
                    </div>
                    <div>
                      <label> Naslov: </label> <br />
                      <input
                        className={`${styles.inp} ${
                          !validateTitle(title) ? styles.error : ""
                        }`}
                        type="text"
                        placeholder="Dodaj naslov"
                        name="title"
                        id="title"
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                      />
                    </div>
                    <br />

                    <div>
                      <label> Opis sastanka: </label> <br />
                      <textarea
                        className={`${styles.inp} ${
                          !validateDesc(description) ? styles.error : ""
                        }`}
                        placeholder="Unesite opis..."
                        name="description"
                        id="description"
                        value={description}
                        style={{ resize: "none" }}
                        onChange={(e) => setDesc(e.target.value)}
                      ></textarea>
                    </div>
                    <br />

                    <div>
                      <label> Izaberite vreme: </label> <br /> <br />
                      <TimeKeeper
                        time={time}
                        name="time"
                        id="time"
                        onChange={(newTime) => setTime(newTime.formatted12)}
                        hour24Mode
                        coarseMinutes={15}
                        forceCoarseMinutes
                        value={time}
                      />
                      <span> {time}</span>
                    </div>
                    <br />
                    <br />

                    <div>
                      <label> Dodaj ucesnike: </label> <br />
                      <MultiSelect
                        options={attendees}
                        value={selected}
                        onChange={setSelected}
                        labelledBy="Select"
                      />
                    </div>
                    <br />
                    <button className={styles.btn2} type="submit">
                      Dodaj
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Form;
