import styles from "./calendar.module.css";
import Form from "./Form";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";

export default function Calendar() {
  const [day, setDay] = useState("1");
  const [id, setId] = useState();
  const [events, setEvents] = useState([]);
  const [show, setShow] = useState(false);
  const router = useRouter();

  const handleDoubleClick = () => {
    setShow(true);
  };

  const dateClickHandler = (day) => {
    setDay(day);
    setId(
      events[events.length - 1].id == undefined
        ? 1
        : events[events.length - 1].id + 1
    );
  };

  const getUsers = async () => {
    const res = await fetch("/event");
    const data = await res.json();
    setEvents([...data]);
  };

  useEffect(() => {
    getUsers();
  }, []);

  const checkForEvent = (day) => {
    const listOfEvents = [];
    for (let e of events) {
      if (e.day == day) {
        listOfEvents.push(e);
      }
    }
    return listOfEvents;
  };

  const days = ["pon", "uto", "sre", "cet", "pet", "sub", "ned"];

  const daysNumbers = [...Array(31).keys()];

  return (
    <div className={styles.container}>
      <h1 className={styles.l}> Avgust 2022 </h1>
      <table style={{ width: "60%" }}>
        <thead className={styles.header}>
          <tr>
            {days.map((day) => (
              <td key={day} style={{ padding: "5px 0" }}>
                <div
                  className={styles.days}
                  style={{ textAlign: "center", padding: "5px 0" }}
                >
                  {day}
                </div>
              </td>
            ))}
          </tr>
        </thead>
        <tbody>
          {daysNumbers
            .filter((_, i) => i % 7 === 0)
            .map((week) => {
              return (
                <tr
                  key={week}
                  style={{ padding: "5px 0" }}
                  onDoubleClick={() => handleDoubleClick()}
                >
                  {daysNumbers.slice(week, week + 7).map((day) => (
                    <td
                      key={day}
                      className={styles.day}
                      style={{
                        textAlign: "center",
                        padding: "5px 0",
                        width: "100px",
                        height: "110px",
                        border: "solid",
                        borderColor: "rgb(136, 170, 233)",
                        fontFamily: "Verdana",
                      }}
                      onClick={() => dateClickHandler(day)}
                    >
                      <div onClick={() => setDay(day)}>{day + 1}</div>
                      <div>
                        {" "}
                        {checkForEvent(day).map((e) => {
                          return (
                            <Link href={`/events/${e.id}`} key={e.id}>
                              <p onClick={() => router.push("/events")}>
                                {e.title + " - vreme: " + e.time}
                              </p>
                            </Link>
                          );
                        })}
                      </div>
                    </td>
                  ))}
                </tr>
              );
            })}
        </tbody>
      </table>
      <div>
        <Form
          show={show}
          id={id}
          day={day}
          onClose={() => {
            setShow(false);
          }}
        />
      </div>
      <div></div>
    </div>
  );
}
