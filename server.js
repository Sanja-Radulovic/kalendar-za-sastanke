const express = require("express");
const next = require("next");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost:27017/events", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection.once("open", function () {
  console.log("Uspesno povezivanje");
});

const EventModel = require("./models/Event");
const AttendeesModel = require("./models/Attendees");

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();
  server.use(bodyParser.json());

  server.get("/event", async (req, res) => {
    const e = await EventModel.find().exec();
    res.send(e);
  });

  server.get("/eventById/:id", async (req, res) => {
    const e = await EventModel.find({ id: parseInt(req.params.id) }).exec();
    res.send(e);
  });

  server.get("/attendees", async (req, res) => {
    res.send(await AttendeesModel.find().exec());
  });

  server.delete("/event", async (req, res) => {
    EventModel.deleteOne({ id: req.body.id }).exec();
    res.json(req.body);
  });

  server.post("/event", async (req, res) => {
    res.json(req.body);
    EventModel.create(req.body);
  });

  server.all("*", (req, res) => {
    return handle(req, res);
  });

  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});
