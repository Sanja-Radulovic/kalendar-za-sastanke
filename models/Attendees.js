const mongoose = require("mongoose");

const AttendeesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Attendees", AttendeesSchema);
