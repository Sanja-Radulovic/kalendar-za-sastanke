const mongoose = require("mongoose");

const EventSchema = new mongoose.Schema({
  id: {
    type: Number,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  day: {
    type: String,
    required: true,
  },
  time: {
    type: String,
    required: true,
  },
  attendees: [
    {
      type: String,
      required: true,
    },
  ],
});

module.exports = mongoose.model("Event", EventSchema);
